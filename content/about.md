+++
title = "About"
date = "2019-06-26"
aliases = ["about-us","about","contact"]
[ author ]
  name = "Robert Lowe"
+++

The task force represents a continuation of the existing task force on Developmental Psychology but involves some different personnel. The combined theoretical and empirical research interests of the task force are complementary and it is hoped that through meetings and organized events significant collaborations that exploit the members’ expertise will be achieved.

## Goals
The goals of this task force are:  

* to disseminate ideas and approaches to the study of developmental psychology
* to promote inter-disciplinary approaches to building cognitive technological systems inspired by insights into cognitive development
* to promote future collaborations studying aspects of developmental cognition and technological and computational applications thereof through conferences, workshops and special issues in
relevant journals.  

## Scope
* Developmental Psychology  
* Developmental Cognitive Robotics 
* Affective systems and development  
* Interactive technological systems and their role in infant development  
  

