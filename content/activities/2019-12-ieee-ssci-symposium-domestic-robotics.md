---
author:
  name: "Robert Lowe"
date: 2019-05-06
LinkTitle: "Wallenberg Symposium on Affective and Developmental Processes in Cognitive and Autonomous Systems"
type:
- activity
- activities
title: "Wallenberg Symposium on Affective and Developmental Processes in Cognitive and Autonomous Systems"
weight: 10
series:
- Symposium
---

Wallenberg Symposium on Affective and Developmental Processes in Cognitive and Autonomous Systems  

Organizers: Robert Lowe and Alexander Almer  
Date: 6<sup>th</sup> to 7<sup>th</sup> of May 2019  
Location: Gothenburg, Sweden  
https://robertlowe2.gitlab.io/wallenbergsymposiumgothenburg2019/  

