---
author:
  name: "Robert Lowe"
date: 2019-08-19
LinkTitle: "Workshop on Naturalistic Non-Verbal and Affective Human-Robot Interactions"
type:
- activity
- activities
title: "ICDL-EpiRob Workshop on Naturalistic Non-Verbal and Affective Human-Robot Interactions"
weight: 10
series:
- Workshops
---

ICDL-EpiRob Workshop on "Naturalistic Non-Verbal and Affective Human-Robot Interactions" at the Joint IEEE International Conference on Development and Learning and Epigenetic Robotics (ICDL-EPIROB)  

Organizers: Nicolás Navarro-Guerrero, Robert Lowe, Chrystopher Nehaniv  
Date: 19<sup>th</sup> August 2019  
Location: Oslo, Norway  
https://nicolas-navarro-guerrero.gitlab.io/workshop-non-verbal-human-robot-interactions-icdl-epirob-2019/  

