+++
title = "Members"
date = "2019-06-26"
aliases = ["Members","contact"]
[ author ]
  name = "Robert Lowe"
+++

## Chair
>**Chrystopher Nehaniv**  
Department of Systems Design Engineering  
University of Waterloo, Canada,  
E-mail: chrystopher.nehaniv@uwaterloo.ca  
URL: https://uwaterloo.ca/systems-design-engineering/about/people/cnehaniv  
>

## Vice-Chairs
>**Robert Lowe**  
Department of Applied IT  
University of Gothenburg, Sweden,
E-mail: robert.lowe@gu.se  
URL: https://www.researchgate.net/profile/Robert_Lowe3 
>

>**Alexander Almer**  
Department of Applied IT  
University of Gothenburg, Sweden,
E-mail: alexander.almer@gu.se  
URL: https://www.gu.se/english/about_the_university/staff/?languageId=100001&userId=xalmea
>

## Members
>Christian Balkenius  
Lund University, Sweden,  
http://christian.balkenius.se  
>

>Linda Smith  
Indiana University, USA,  
http://www.iub.edu/~cogdev/
>

>John Spencer  
University of East Anglia, UK  
https://www.facebook.com/DDPSYUEA
>

>Jun Tani  
Okinawa Institute of Science and Technology (OIST), Japan,  
https://groups.oist.jp/cnru/jun-tani
>

>Nicolas Navarro-Guerrero  
Deutsches Forschungszentrum für Künstliche Intelligenz GmbH (DFKI), Germany  
https://nicolas-navarro-guerrero.github.io
>

>Mariska Kret  
University of Leiden, Netherlands  
https://www.universiteitleiden.nl/en/staffmembers/mariska-kret#tab-1
>

>Hatice Gunes  
University of Cambridge, UK  
https://www.cl.cam.ac.uk/~hg410/
>

>Yulia Sandamirskaya  
University of Zurich / ETH Zurich, Switzerland,  
http://www.sandamirskaya.eu
>

