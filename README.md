# Task Force on Developmental Psychology
## IEEE CIS Technical Committee on Cognitive and Developmental Systems

https://cdstc.gitlab.io/task-force-on-developmental-psychology

Copyright (C) 2019 [Nicolas Navarro-Guerrero](https://nicolas-navarro-guerrero.github.io/)  
Contact:  nicolas.navarro.guerrero@gmail.com  
        https://nicolas-navarro-guerrero.github.io/


Based on the [Hugo Theme hello-friend-ng](https://themes.gohugo.io/hugo-theme-hello-friend-ng/) by [Djordje Atlialp](https://github.com/rhazdon/hugo-theme-hello-friend-ng).

[Example Page](https://themes.gohugo.io/theme/hugo-theme-hello-friend-ng)

